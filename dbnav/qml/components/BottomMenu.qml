import QtQuick 2.9
import Morph.Web 0.1
import Ubuntu.Components 1.3
import QtQuick.LocalStorage 2.0
import Ubuntu.Components.Popups 1.3
import Ubuntu.Content 1.1
import QtQuick.Controls 2.3

Rectangle {
  id: bottomMenu
  z: 100000
  width: parent.width
  height: units.gu(4)
  color: "#cccccc"

  anchors {
    top: parent.top
  }

  /*Rectangle {
    width: parent.width
    height: units.gu(0.1)
    color: UbuntuColors.lightGrey
  }*/

  Row {
    width: parent.width
    height: parent.height-units.gu(0.1)
    anchors {
      centerIn: parent
    }

    Item {
      width: parent.width/4
      height: parent.height

      Icon {
        anchors.centerIn: parent
        width: units.gu(2.2)
        height: width
        name: "go-previous"
        color: "#333333"
      }

      MouseArea {
        anchors.fill: parent
        onClicked: {
          webview.goBack()
        }
      }
    }

    Item {
      width: parent.width/4
      height: parent.height

      Icon {
        anchors.centerIn: parent
        width: units.gu(2.2)
        height: width
        name: "reload"
        color: "#333333"
      }

      MouseArea {
        anchors.fill: parent
        onClicked: {
          webview.reload()
        }
      }
    }

    Item {
      width: parent.width/4
      height: parent.height

      Icon {
        anchors.centerIn: parent
        width: units.gu(2.2)
        height: width
        name: "go-next"
        color: "#333333"
      }

      MouseArea {
        anchors.fill: parent
        onClicked: {
          webview.goForward()
        }
      }
    }

    Item {
      width: parent.width/4
      height: parent.height




      Button {
          id: fileButton
          text: "Nav"
          anchors.centerIn: parent


          onClicked: menu.open()


          Menu {
              id: menu
              y: fileButton.height


              Action {
                  text: "Auskunft und Reservierung"
                  onTriggered: webview.myMobileUrl = "https://mobile.bahn.de/bin/mobil/query.exe/dox?country=DEU&rt=1&use_realtime_filter=1&webview=&searchMode=NORMAL"
              }
              Action {
                  text: "Meine Bahn"
                  onTriggered: webview.myMobileUrl = "https://accounts.bahn.de/"
              }
              Action {
                  text: "Von Tür zu Tür"
                  onTriggered: webview.myMobileUrl = "https://mobile.bahn.de/bin/mobil/query.exe/dox?country=DEU&rt=1&use_realtime_filter=1&webview=&searchMode=ADVANCED"
              }
              Action {
                  text: "Ankunft / Abfahrt"
                  onTriggered: webview.myMobileUrl = "https://mobile.bahn.de/bin/mobil/bhftafel.exe/dox?country=DEU&rt=1&use_realtime_filter=1&webview=&"
              }
              Action {
                  text: "Ist mein Zug pünktlich?"
                  onTriggered: webview.myMobileUrl = "https://mobile.bahn.de/bin/mobil/trainsearch.exe/dox?webview=&"

              }
              Action {
                  text: "Auftragssuche"
                  onTriggered: webview.myMobileUrl = "https://fahrkarten.bahn.de/mobile/ru/rs.post?&sc=suche#stay"
              }
              Action {
                  text: "Baustelleninfo"
                  onTriggered: webview.myMobileUrl = "https://bauinfos.deutschebahn.com/"
              }
          }
      }












    }

  }
}
